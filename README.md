
this project is part of Declarations, an artistic research into the poetic materiality of the CSS web-standard.

this is an under development extension to collect and curate scrap of CSS around the web.

to use

1. first install node:

        sudo pacman -S nodejs npm

2. install web-ext

        sudo npm install --global web-ext

3. run your extension, inside of the folder of the extension

        web-ext run

## external tools

* filesaver
* https://specificity.keegan.st/, compiled using browserify

## about this tool

to write

* Declarations context (Doriane)
* What this tool does
  * what question do we imagine this can answer about CSS materiality
  * the notion and idea of scrap
  * ...
* Explain how it was imagined (Natalia, Doriane)
  * the pipette / scisors / spyglass / bookmark / camera metaphors
  * references to other visual collections of scraps
  * the obvious problems when CSS is decontextualise (we loose the background, the fonts, the inheritance, the responsivity, do we only keep a screenshot, an iframe of the website itself, what if it changes?)
* Explaining how scrapping CSS is done (Doriane)
  * 3 ways of accessing CSS styles
    * `.style` inline
    * `.computedStyle` frozen computed values
    * CSSStylesheetAPI
  * pipette (with CSS recursivity) - current element style
  * seringe (with CSS recursivity, HTML recursivity) - current element style, and all of its descendant
  * spyglass (with CSS recursivity, HTML recursivity) - current element style and all of its inherited style, and all of its descendant
  * note on specificity
* Explaining how screenshoting is done (Natalia, Einar)
  * the problem of current situation
  * differences between local screenshot and server side screenshot
  * extension javascript capture API


## todo

* [x] :root variable
* [x] merge stylesheets into one
* [x] grab the last parent with same clientbounding box
* [x] grab scrap size
* [x] rewrite url of img and script and everything with a src attribute
* [ ] inherited styles
* [ ] CORS restriction
* [ ] insert original stylesheet as comment

* [ ] create popup instead of alert before scrapping
  * [ ] fullname of scrap
  * [ ] max deepness of childs (show warning if more than 5)
  * [ ] field to enter scrapper
  * [ ] field to enter keyword
  * [ ] scrap button to send (local, online, both)

* [ ] rewrite url of background and fonts
* [ ] check for :hover and remove current state when scrapping
* [ ] add the :after, :before
* [ ] only keep used @font-face and @keyframe
* [ ] specificity (gen_class remove priorities...)
  * [ ] save specificity and comment
  * [ ] print comment inside of outputs
  * [ ] reorder tree according to specificity

## to test

create some kind of acid test that has to pass those test?

* pseudo elements
* state like hover, active, focus, visited, ...
* animations
* custom properties in :root
* two nested div with same size (should grab latest ancestor)
* inherited styles
* inherited custom properties
* inherited keyword
* specificity
* CORS (other domain stylesheet)