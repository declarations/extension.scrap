function downloadImage(imageURI, filename) {
  // imageURI is a base64 encoded image
  const imageAnchor = document.createElement("a")
  imageAnchor.href = imageURI;
  imageAnchor.download = `${filename}.png`;
  imageAnchor.click();
  imageAnchor.remove()
}

browser.runtime.onMessage.addListener((message) => {
  if (message.command === "screenshotArea") {
    console.log('message received')
    const image = captureAndDownloadScreenshot(message.area, message.scroll, message.filename)
    return Promise.resolve(image);
  };
});

async function captureAndDownloadScreenshot(area, scroll, filename) {
  const { x, y, width, height } = area
  const options = {
    rect: {
      x: x + scroll.x,
      y: y + scroll.y,
      width,
      height,
    },
  }
  const imageURI = await browser.tabs.captureTab(options)
  downloadImage(imageURI, filename)

  let imageBlob = await fetch(imageURI).then(
    res => res.blob()
  )

  return imageBlob;
}

