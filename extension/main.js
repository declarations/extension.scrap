
const apiUrl = 'http://localhost:8000';

let all = document.querySelectorAll("*");

// properties inherited by default according to 
// https://stackoverflow.com/questions/5612302/which-css-properties-are-inherited
let inherited = [
    'border-collapse',
    'border-spacing',
    'caption-side',
    'color',
    'cursor',
    'direction',
    'empty-cells',
    'font-family',
    'font-size',
    'font-style',
    'font-variant',
    'font-weight',
    'font-size-adjust',
    'font-stretch',
    'font',
    'letter-spacing',
    'line-height',
    'list-style-image',
    'list-style-position',
    'list-style-type',
    'list-style',
    'orphans',
    'quotes',
    'tab-size',
    'text-align',
    'text-align-last',
    'text-decoration-color',
    'text-indent',
    'text-justify',
    'text-shadow',
    'text-transform',
    'visibility',
    'white-space',
    'widows',
    'word-break',
    'word-spacing',
    'word-wrap',
]

//  RECURSIVE STYLESHEET WALKER
//  -------------------------------------------------------------------

function copyStyle(stylesheet){
    let new_style = new CSSStyleSheet();
    for (let rule of stylesheet.cssRules){
        // serialize each rules by dumping to text
        new_style.insertRule(rule.cssText, new_style.cssRules.length);
    }
    return new_style;
}

function mergeStyle(stylesheets){
    let new_style = new CSSStyleSheet();
    for (let stylesheet of stylesheets){
        try {
            rules = stylesheet.cssRules;
            for(let rule of stylesheet.cssRules){
                new_style.insertRule(rule.cssText, new_style.cssRules.length);
            }
        }
        catch(err) {
            let info = stylesheet.href + "\n\n" + err;
            alert(info);
        } 
    }
    return new_style;
}

function printStyle(stylesheet){
    text_style = "";
    for (let rule of stylesheet.cssRules){
        text_style = text_style + rule.cssText + "\n\n";
    }
    return text_style;
}

// NOTE:
// printStyle(styleSheets) == printStyle(copyStyle(styleSheets));
// == True

function removeRule(rules, i){
    let parent = rules[i].parentRule ? rules[i].parentRule : rules[i].parentStyleSheet;
    parent.deleteRule(i);
    return i - 1;
}

function filter(rules, target, gen_class){

    for (let i = 0; i < rules.length; i++) {

        // "The CSSGroupingRule interface of the CSS Object Model represents any CSS
        // at-rule that contains other rules nested within it."
        // https://developer.mozilla.org/en-US/docs/Web/API/CSSGroupingRule
        // this includes @media, @support, etc
        if (rules[i] instanceof CSSGroupingRule && rules[i].cssRules.length > 0){

            // (recursivity)
            filter(rules[i].cssRules, target, gen_class);

            // (post-recursivity)
            // REMOVE if we removed everything in the group
            if (rules[i].cssRules.length == 0){
                i = removeRule(rules, i);
            }
        }

        // either not a group
        // either an empty group 
        // NOTE: styleRule can be empty groupingRule
        else{
            if (rules[i] instanceof CSSStyleRule){

                if (rules[i].selectorText == ':root'){
                    // we keep the styleRule
                    // and don't normalize the selector
                }
                // REMOVE if it's a styleRule with a selector 
                // that doesn't match the target
                else if (! target.matches(rules[i].selectorText)){
                    i = removeRule(rules, i);
                }
                else{
                    // we keep the styleRule
                    // let's normalize the selector
                    
                    // we keep the one that match with highest specificity
                    let selectors = rules[i].selectorText.split(',');
                    let specificities = []
                    for (let selector of selectors){
                        if(target.matches(selector)){
                            specificities.push(specificity.calculate(selector));
                        }
                    }
                    specificities.sort(specificity.compare).reverse();

                    rules[i]['original_selector'] = rules[i].selectorText;
                    rules[i]['specificity'] = specificities[0];

                    rules[i].selectorText = "." + gen_class;
                    // console.log(rules[i].original_selector);
                    // console.log(rules[i].specificity);
                }
            }
        }
    }
}

function recursivePrint(rules){

    let txt_rules = ''

    for (let i = 0; i < rules.length; i++) {
        let txt;
        if (rules[i] instanceof CSSGroupingRule && rules[i].cssRules.length > 0){

            // (recursivity)
            txt = recursivePrint(rules[i].cssRules);

            // (post-recursivity)
            txt_rules += txt + '\n\n';
        }
        else{
            if (rules[i] instanceof CSSStyleRule){

                txt = '/* \n';
                txt += rules[i]['original_selector'] + '\n';
                txt += rules[i]['specificity'] + '\n';
                txt += '*/ \n';

                // console.log(rules[i]['original_selector']);
                // console.log(rules[i]['specificity']);

                txt += rules[i].cssText;

                txt_rules += txt + '\n\n';
            }
        }
    }

    return txt_rules;
}


//  ONE FUNCTION PER THINGS WE SCRAP
//  -------------------------------------------------------------------

function bookmark(){
    return [window.location.href, window.location.hostname];
}

function etiquette(target, url){
    let id = target.hasAttribute('id') ? '#' + target.id : '' ;
    let tag = target.tagName;
    let classes =  target.classList != '' ? '(.' + String(target.classList).replaceAll(' ', '.') + ')' : '';
    return  tag + '|' + url + id + classes;
}

function scissor(target){
    return target.outerHTML;
}

function pipette(stylesheet, target, gen_class="scrap"){
    // adding generated classe and normalizing selectors
    target.classList.add(gen_class);

    // to not break the image source and other we need to convert every src to absolute
    if (target.hasAttribute('src')){
        let src = new URL(target.src, document.baseURI).href;
        target.src = src;
        console.log('normalized src');
    }
    if (target.hasAttribute('srcset')){
        let srcset = target.srcset.split(',');
        for (let i=0; i < srcset.length; i++){
            let src = srcset[i].trim().split(' ');
            src[0] = new URL(src[0], document.baseURI).href;
            src = src.join(' ');
            srcset[i] = src;
        }
        target.srcset = srcset.join(',');
        console.log('normalized srcset');
    }

    // cloning styles in order to remove from it
    let clone = copyStyle(stylesheet);

    // removing rules that don't apply
    filter(clone.cssRules, target, gen_class);

    // return filtered clone
    return clone;
}

function syringe(stylesheet, target, gen_class="scrap"){
    // given an element, and a scrap object
    // does the pipette on itself and every of its child

    // pipetting current element
    let selected_styles = pipette(stylesheet, target, gen_class);

    // repeating same process on children
    let children = target.children;
    for (let i = 0; i < children.length; i++) {

        // (recursivity)
        let new_gen_class = gen_class + '-' + String(i);
        let children_styles = syringe(stylesheet, children[i], new_gen_class);

        // (post-recursivity)
        // adding the selected child style to the parent ones
        for(let rule of children_styles.cssRules){

            // only add it if it's not already in
            let text_style = printStyle(selected_styles);
            if(! text_style.includes(rule.cssText)){
                selected_styles.insertRule(rule.cssText, selected_styles.cssRules.length);
            }
        }
    }

    return selected_styles;
}

// get the inherited style
function spyglass(el, scrap){

    // going to parent element
    let parent = el.parentElement;

    if (parent){
        console.log(parent);

        let parent_style = pipette(parent);
        for(let rule of parent_style){
            console.log(rule.cssText);

            // if there are any --custom-variable grab them
        }
    
        spyglass(parent, scrap);
    }
}

function meter(el){
    // Get the dimension of the element that will be scrapped
    return el.getBoundingClientRect();
}

function enclose(el){
    // grab latest parent with same size
    let parent = el.parentElement;
    if( meter(parent).width == meter(el).width 
        && meter(parent).height == meter(el).height ){
        console.log('enclosing to', parent);
        el = enclose(parent);
    }
    return el;
}

async function camera(el, scrapName){
    // Get the dimension of the element that will be scrapped
    const screenShotArea = el.getBoundingClientRect();
    // Send a message to the background script.
    let imageBlob = await browser.runtime.sendMessage({
        command: "screenshotArea",
        area: screenShotArea,
        filename: scrapName,
        scroll: {x: window.scrollX, y: window.scrollY}
    });

    imageBlob.name = 'lol.png';
    console.log(imageBlob);

    return imageBlob
}


//  ADDING THE SHIFT VIEW ON SHIFT PRESS
//  -------------------------------------------------------------------

function isShiftOn(event){
    ct = event.keyCode ? event.keyCode:event.which;
    sh = event.shiftKey ? event.shiftKey:((ct == 16) ? true:false);
    return sh;
}

// appending interface style to the page
let shift_view_style = `
.shift-view *:hover{
    outline: 2px dotted aqua !important;
    cursor: crosshair !important;
}
.shift-view ::selection{
    background-color: transparent !important;
}
`
let shift_view_stylesheet = document.createElement("style");
shift_view_stylesheet.innerText = shift_view_style;
document.head.appendChild(shift_view_stylesheet);

// toggelling the interface on the body at keyPress
window.addEventListener("keydown", function(event){
    if(isShiftOn(event)){
        document.body.classList.add('shift-view');
    }
});
window.addEventListener("keyup", function(event){
    document.body.classList.remove('shift-view');
});


//  API REQUEST TO ADD A SCRAP
//  -------------------------------------------------------------------

function postScrap(scrap){

    let data = new FormData()
    for (key in scrap) {
        if(key == 'screenshot'){
            data.set(key, scrap[key], 'screenshot.png');
        }
        else{
            data.set(key, scrap[key]);
        }
    }
    
    fetch(`${apiUrl}/api/scrap/`, {
        'method': 'POST',
        'mode': 'cors', 
        'body': data
      })
      .then(r => {
        r.json()
          .then(scrap => {
            console.log('Created scrap', scrap);
            alert('scrap saved to database');
          })
          .catch(e => console.log('Something went wrong', e));
      })
      .catch(e => console.log('Something went wrong', e));
}

function dlScrap(scrap){
    // download scrap as raw html + css
    let blob = new Blob([scrap['html'], "\n\n<style>" + scrap['normalizedStyle'] + "</style>"], {type: "text/plain;charset=utf-8"});
    saveAs(blob, scrap['name'] + '.html');
}


//  ADDING THE SCRAPPING ON CLICK TO ALL ELEMENTS
//  -------------------------------------------------------------------

function scrapElement(event){
    if(isShiftOn(event)){

        // we want to capture only the actually clicked element, without the parents
        // https://javascript.info/bubbling-and-capturing#event-target
        event.preventDefault();
        event.stopPropagation();
        let target = event.target;

        // grab latest parent with same size
        target = enclose(target);
        
        // remove interface
        document.body.classList.remove('shift-view');

        // archiving everything into a scrap object
        let scrap = {};
        
        // grabbing URL information
        let urls = bookmark();
        
        // technichal informations
        // and showing a popup to fill more
        scrap['url'] = urls[0];
        scrap['domain'] = urls[1];
        scrap['name'] = etiquette(target, scrap['url']);
        
        // --> popup interuption -->
        window.alert(scrap['name']);
        // --> when popup closes -->

        // take a screenshot of the clicked element 
        // (async)

        // take the current width and height of the scrap
        // for collection display
        let size = meter(target);
        scrap['width'] = size.width;
        scrap['height'] = size.height;

        
        // merge all the stylesheets to one stylesheet
        let styles = mergeStyle(document.styleSheets);
        
        // filtering all the declared styles on the target
        // and all of its children
        let selected_style = syringe(styles, target);
        
        // spyglass with inherited styles goes here
        // ...

        // IMPORTANT: html must be grabbed after class normalization
        // taking the current html of the target
        scrap['html'] = scissor(target);
        
        // filling in the different type of styles
        scrap['declaredStyle'] = printStyle(selected_style);
        scrap['normalizedStyle'] = printStyle(selected_style);
        scrap['inheritedStyle'] = printStyle(selected_style);
        scrap['inlineStyle'] = target.style;

        camera(target, scrap['fullname']).then(imageURIResponse => {
            scrap['screenshot'] = imageURIResponse;

            // download scrap locally as HTML file
            dlScrap(scrap);

            // post scrap to the online collection
            postScrap(scrap);
        });
    }
}


(() => {
    /**
    * Check and set a global guard variable.
    * If this content script is injected into the same page again,
    * it will do nothing next time.
    * ---
    * Not sure if this is still needed but why not??
    */
    if (window.hasRun) {
      return;
    }
    window.hasRun = true;

    for (let el of all){
        el.addEventListener('click', (event) => scrapElement(event));
    }

})();
