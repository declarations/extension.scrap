from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.views.decorators.clickjacking import xframe_options_exempt

from .models import Scrap

from .serializers import ScrapSerializer
from rest_framework import viewsets


class ScrapViewset(viewsets.ModelViewSet):
    queryset = Scrap.objects.all().order_by("-createdAt")
    serializer_class = ScrapSerializer


def index (request):
    return render(
        request,
        "index.html",
        {
            'scraps': Scrap.objects.all().order_by("-createdAt")
        }
    )

@xframe_options_exempt
def iframe (request, pk):
    return render(
        request,
        "iframe.html",
        {
            'scrap': get_object_or_404(Scrap, pk=pk)
        }
    )