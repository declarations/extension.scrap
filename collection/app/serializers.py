from .models import Scrap
from rest_framework import serializers

class ScrapSerializer(serializers.ModelSerializer):
  class Meta:
    model = Scrap
    fields = ['pk', 'createdAt', 'name', 'domain', 'url', 'width', 'height', 'declaredStyle', 'normalizedStyle', 'inheritedStyle', 'inlineStyle', 'html', 'screenshot']