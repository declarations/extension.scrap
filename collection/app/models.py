from django.db import models
import uuid
import os

def create_filename(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('images/', filename)

# Create your models here.
class Scrap (models.Model):
    # Practically maximum length url is 2000 chars
    # https://stackoverflow.com/a/417184

    createdAt = models.DateTimeField(auto_now_add=True)
    
    # meta
    name = models.CharField(max_length=2000)
    domain = models.CharField(max_length=1000)
    url = models.URLField(max_length=2000)
    width = models.FloatField()
    height = models.FloatField()

    # styles
    declaredStyle = models.TextField()
    normalizedStyle = models.TextField()
    inheritedStyle = models.TextField()
    inlineStyle = models.TextField()

    html = models.TextField()
    screenshot = models.ImageField(upload_to=create_filename, null=True, blank=True)

    # user => ForeignKey()