# Generated by Django 5.0.2 on 2024-02-23 16:35

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Scrap',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('createdAt', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(max_length=2000)),
                ('fullname', models.CharField(max_length=4000)),
                ('domain', models.CharField(max_length=1000)),
                ('fullUrl', models.URLField(max_length=2000)),
                ('declaredStyle', models.TextField()),
                ('normalizedStyle', models.TextField()),
                ('normalizedWithInheritedStyle', models.TextField()),
                ('html', models.TextField()),
                ('screenshot', models.ImageField(blank=True, null=True, upload_to='')),
            ],
        ),
    ]
