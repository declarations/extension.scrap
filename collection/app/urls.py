from . import views
from django.urls import include, path
from rest_framework import routers
from django.contrib import admin

router = routers.DefaultRouter()
router.register(r'scrap', views.ScrapViewset)

urlpatterns = [
  path('', views.index, name='index'),
  path('iframe/<int:pk>/', views.iframe, name='iframe'),
  path('api/', include(router.urls)),
  path('admin/', admin.site.urls)
]