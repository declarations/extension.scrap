# Scrap API

Web application allowing to browse scraps as produced with the browser extension, and an API to store them programatically.

The application is based on [Django](https://www.djangoproject.com/) and [Django rest framework](https://www.django-rest-framework.org/)

## Installation

All steps assume an open terminal, `cd`'ed into the folder containing this readme.

First, install the requirements. For example through pip:
```
pip install -r requirements.txt
```

Then, to set up a database run the migrate command through the django management script:
```
python manage.py migrate
```

*Note:* by default the application will use sqlite3. The database file `db.sqlite3` will be made in the same folder as this readme. To adjust the name, location, or engine of the database adjust the file `settings.py`. See also the [documentation of Django](https://docs.djangoproject.com/en/4.2/ref/settings/#databases).

## Running 

During development it is the easiest to the start the application through the django management script:

```
python manage.py runserver
```

The web interface should be accesible at: <http://localhost:8000/>

The web interface for the scrap API should be accesible at: <http://localhost:8000/api/scrap/>

*Note:* it is possible to bind the development server to a [different ip address and / or port](https://docs.djangoproject.com/en/4.2/ref/django-admin/#examples-of-using-different-ports-and-addresses). By binding to 0.0.0.0 the API will be accessible within the local network (at the ip-address of the device):

```
python manage.py runserver 0.0.0.0:8000
```

## Using the API

### List

This snippet will fetch the scraps from the server and log in the console.

```javascript
const apiUrl = 'http://localhost:8000';
fetch(`${apiUrl}/api/scrap/`, { 'method': 'GET', 'mode', 'cors' })
  .then(r => {
    r.json()
      .then(scraps => {
        scraps.forEach(console.log)
      })
      .catch(e => console.log('Something went wrong', e));
  })
  .catch(e => console.log('Something went wrong', e));
```

### Post

This snippet will make a post request and create a snippet.

```javascript
const apiUrl = 'http://localhost:8000';

let scrap = {
  'fullname': 'Programmatically created scrap',
  'name': 'Programmatically created scrap',
  'domain': 'http://localhost',
  'fullUrl': 'http://localhost:8000',
  'declaredStyle': 'h1 { color: red; }',
  'normalizedStyle': 'h1 { color: red; }',
  'normalizedWithInheritedStyle': 'h1 { color: red; }',
  'html': '<h1>Hello world</h1>',
}

let data = new FormData()
for (key in scrap) {
  data.set(key, scrap[key]);
}

fetch(`${apiUrl}/api/scrap/`, {
    'method': 'POST',
    'mode': 'cors', 
    'body': data
  })
  .then(r => {
    r.json()
      .then(scrap => {
        console.log('Created scrap', scrap);
      })
      .catch(e => console.log('Something went wrong', e));
  })
  .catch(e => console.log('Something went wrong', e));
```

## API Fields
```
{
    'pk': integer,
    'createdAt': string,
    'fullname': string,
    'name': string,
    'domain': string,
    'fullUrl': string,
    'declaredStyle': string,
    'normalizedStyle': string,
    'normalizedWithInheritedStyle': string,
    'html': string,
    'screenshot': string | null
}
```

## API Endpoints

### `GET /api/scrap/`
List scraps. Returns an array of scraps.

### `GET /api/scrap/<int:pk>/`
Get individual scrap.

### `POST /api/scrap/`
Creates a new scrap.

### `PUT /api/scrap/<int:pk>/`
Update individual scrap.

### `DELETE PUT /api/scrap/<int:pk>/`
Delete individual scrap.

## CORS

By default the API allows all origins.